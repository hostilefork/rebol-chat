Rebol [
    title: "Demo Usage"
    Notes: {
       Let's see if we can repro the lib/write bug
    }
    date: 6-Feb-2020
    version: 0.0.31
]

probe system/script/header/version
?? pwd

; import <httpd>
import https://raw.githubusercontent.com/gchiu/rebol-httpd/master/httpd.reb ; https://github.com/gchiu/rebol-httpd/blob/master/httpd.reb

w*: :lib/write

logfile: %logs/loga.txt 
if not exists? logfile [lib/write logfile ""]

srv-block: [
    scheme: 'httpd 80
]

process-requests: [
    -- request/action
    ; lib/write/append logfile spaced [now/precise request/action newline]
    w*/append logfile spaced [now/precise request/action newline]
    -- "Post log update"
    switch request/method [
        "GET" [
            -- "got a get"
            render to text! lib/read %index.html
        ]
        
        "POST" [
        ]
        
        "PUT" [
        ]

        "DELETE" [
        ]
    ]
    response/set-cookie: default [copy "Nothing here folks"]
]

insert/only tail srv-block process-requests

srv: open srv-block
    
print "Serving on port 80"    
if err: trap [
    wait [srv]
][
    w*/append logfile spaced [now/precise mold err newline]
    ; lib/write/append logfile spaced [now/precise mold err newline]
]
