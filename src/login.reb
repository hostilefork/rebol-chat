Rebol [
    title: "Login functions to chat"
    file: %login.reb
    date: 24-Feb-2020
    notes: {send email address to login page, and gets a session key return, encrypted with your own public key
        call gpg to decrypt the key
    }
]

import <webform>

; chat-server: http://35.224.174.22

accounts: copy []

chat-account: make object! [
    site: _
    sessionkey: _
    username: _
    email: _
    room: _
]

; returns the chat-account object now
login: func [ chat-server [url!] username [word!] email [email!]
    <local> login-address payload response script sessionkey phrase chat-obj
][
    login-address: join chat-server "/login"

    payload: join "email=" url-encode email 
    response: write login-address reduce ['POST payload] 
    
    response: to text! response
    if parse response [ "userid:" any space copy userid numbers thru "sessionkey:" thru "-----BEGIN PGP MESSAGE-----" newline newline thru "-----END PGP MESSAGE-----"][
        cycle [
            prin "Enter your GPG decryption phrase (Q=quit): "
            phrase: input
            if phrase = "Q" [halt]
            if 7 < length-of phrase [break]
        ]
    ] else [
        print response
        return
    ]
    write %challenge.gpg remove/part response find response "-----BEGIN PGP MESSAGE-----"
    attempt [delete %challenge.txt]
    script: spaced ["gpg --pinentry-mode=loopback --passphrase" phrase "-d -o challenge.txt challenge.gpg"] 
    call script
    if exists? %challenge.txt [
        response: to text! read %challenge.txt
        delete %challenge.txt
        if find accounts chat-server [remove/part find accounts chat-server 2]
        append accounts chat-server
        append accounts chat-obj: make chat-account compose [site: (chat-server) userid: (to integer! userid) sessionkey: (response) email: (email) username: (form username)]
        probe accounts
        chat-obj
    ] else [
        print "Failed to decrypt message"
    ]
]