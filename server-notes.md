# Setup on Amazon

* Signup for a $3.50 [Lightsail](https://aws.amazon.com/lightsail/) linux server (Amazon image)

# Setup on Google Compute Cloud (free tier)

* This is free forever if you follow the instructions carefully
* Create a F1 microserver based in us-central1a ( us-east1, us-west1, and us-central1)
* See  https://cloud.google.com/free/docs/gcp-free-tier

## Software Setup

* SSH into your instance (using the cloud consoles for AWS or GCC)
* Set up the gpg keys `gpg --gen-key`
* Download rebol for linux using the script below, and move the binary to /sbin/

 
```
wget https://dd498l1ilnrxu.cloudfront.net/travis-builds/0.4.40/r3-4ad92eb
mv r3-4ad92eb r3
chmod +x r3
sudo mv r3 /sbin/
```

r3 is now available everywhere.  Let's create the chat directories

```
cd /home/ec2-user
mkdir chatserver
mkdir chatserver/www
mkdir chatserver/www/logs
mkdir chatserver/www/rooms
mkdir chatserver/www/rooms/1
```

now we are going to run the server from the `www` directory

```
cd chatserver/www/
```

and fetch the web server script

```
r3
write %server.reb read https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/src/server.reb
q ; to quit rebol
```

and then run it with nohup.  Without __nohup__ the server closes when your ssh session finishes

```
nohup sudo /sbin/r3 server.reb &              # clearly not good, we'll setup a web server user soon
```

## Terminate Server

* Login using SSH
* Find the running server process `ps aux | grep r3`
* Terminate the process using `sudo kill pid` where the PID is taken from `ps`
* Start a new server after updating it

