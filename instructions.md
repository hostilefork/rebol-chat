# Instructions

To join the chat we need to confirm who you are.  

## Authentication

* In the users folder, create file with your `username.md`
* At the top of the file put your PGP public key
* Below the public key add an encrypted message with

- password: ...
- full name: ...
- email: ...
- username: ...

in that format with the field names so the chat server can parse out your details and add to the chat system.

Use our [public key](rebolchat.asc) which you need to import.  It's under `unspaced ['compkarori '+ 'chat "@gmail.com"]`

```
wget https://gitlab.com/Zhaoshirong/rebol-chat/-/raw/master/rebolchat.asc
gpg import rebolchat.asc
```
and then encrypt your registration.txt file like this

```
gpg --recipient compkarori+chat@gmail.com --armour --encrypt registration.txt
```
which produces a file `registration.txt.asc`

If this won't work, you'll just get a silent fail.

Create an issue if you're having problems.


